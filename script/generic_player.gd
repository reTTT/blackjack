#extends "res:/script/hand.gd"
extends "hand.gd"

export(int, "user", "cpu", "house") var ai = 0
export var name = "player"

func get_ai():
	return ai

func is_busted():
	return get_total() > 21

func shoot():
	return get_total() <= 16

func bust():
	print("player: " + name + " bust.")
