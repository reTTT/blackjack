extends Node2D

var cards = []
export var card_offset = Vector2(40, 0)
export var max_x_offset = 16

func _init():
	add_user_signal("update_card", ["hand"])


func all_face_up():
	for c in cards:
		c.set_face_up(true)
	
	apply()


func add_card(card):
	cards.push_back(card)
	add_child(card)
	apply()


func get_card_offset():
	var s = cards.size()
	var x = (1.0 - (float(s) / 52.0)) * 128

	if x > max_x_offset:
		x = max_x_offset

	return Vector2(x, 0)


func apply():
	var pos = card_offset
	
	for c in cards:
		remove_child(c)
		add_child(c)
		c.set_pos(pos)
		pos += get_card_offset()
	emit_signal("update_card", self)


func clear():
	for c in cards:
		remove_child(c)

	cards.clear()


func get_total():
	if cards.empty():
		return 0

	#if a first card has value of 0, then card is face down; return 0
	if cards[0].get_value() == 0:
		return 0;

	#add up card values, treat each ace as 1
	var total = 0;
	
	for c in cards:
		total += c.get_value()

	#determine if hand contains an ace
	var contains_ace = false;
	for c in cards:
		if c.get_value() == c.R_ACE:
			contains_ace = true

	#if hand contains ace and total is low enough, treat ace as 11
	if contains_ace && total <= 11:
		#add only 10 since we've already added 1 for the ace
		total += 10

	return total


func _ready():
	apply()

