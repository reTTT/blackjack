extends Label

func _on_update(hand):
	var msg = hand.name
	var score = hand.get_total()
	
	if score > 0:
		msg += ": " + str(score)
	
	set_text(msg)


func _ready():
	get_parent().connect("update_card", self, "_on_update" )
	_on_update(get_parent())