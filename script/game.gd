extends Node

const WIN_SCORE = 21

var deck
var house
var players = []

var player_idx = 0


var dlg_result
var dlg_hit

func _ready():
	rand_seed(OS.get_unix_time())
	randomize()

	deck = get_node("world/deck")
	house = get_node("world/house")
	players = get_tree().get_nodes_in_group("player")
	
	dlg_result = get_node("dlg/result")
	dlg_hit = get_node("dlg/hit")

	new_game()


func _process(dt):
	pass


func new_game():
	deck.populate()
	deck.shuffle()

	dlg_result.hide()
	dlg_hit.hide()
	player_idx = 0

	for i in range(2):
		for p in players:
			deck.deal(p).flip()
			p.apply()

		deck.deal(house)
	
	house.flip_first_card()
	
	house.apply()
	deck.apply()
	
	turn()


func turn():
	var player = players[player_idx]
		
	if player.is_busted() || player.get_total() == WIN_SCORE:
		end_turn()
	elif player.ai == 0:
		dlg_hit.show()
	else:
		deck.additional_cards(player, true)
		player.apply()
		end_turn()


func end_turn():
	player_idx += 1
	
	if player_idx >= players.size():
		house.all_face_up()
		deck.additional_cards(house, true)
		deck.apply()
		house.apply()
		show_result()
	else:
		dlg_hit.hide()
		turn()


func show_result():
	var msg
	
	if !house.is_busted():
		if !players[0].is_busted():
			if players[0].get_total() > house.get_total():
				msg = "You win"
			elif players[0].get_total() == house.get_total():
				msg = "At its"
			else:
				msg = "You lose"
		else:
			msg = "You lose"
	else:
		if !players[0].is_busted():
			msg = "You win"
		else:
			msg = "At its"

	dlg_result.get_node("msg").set_text(msg)
	dlg_result.show()


func _on_yes_pressed():
	var player = players[player_idx]

	if !player.is_busted():
		var card = deck.deal(player)
		if card:
			card.flip()

		player.apply()
		deck.apply()

		if player.is_busted():
			player.bust()
			end_turn()
		elif player.get_total() == WIN_SCORE:
			end_turn()


func _on_no_pressed():
	end_turn()


func _on_continue_released():
	house.clear()

	for p in players:
		p.clear()
	
	deck.clear()
	new_game()
